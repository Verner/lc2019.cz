#!/usr/bin/env python3

import email
import hashlib
import io
import json
import os
import re
import sys
import subprocess
import traceback
import unicodedata
import unidecode

from email.utils import unquote
from pathlib import Path


from PyQt5.QtCore import Qt, QStringListModel
from PyQt5.QtWidgets import QApplication, QCompleter, QLineEdit


def get_abs_by_email(data, email):
    for abs_num, abs in enumerate(data['abstracts']):
        if abs['main_author'].get('email', None) == email:
            return abs_num, abs
    return None, None

def get_abs_by_name(data, name):
    for abs_num, abs in enumerate(data['abstracts']):
        if abs['main_author'].get('name', None) == name:
            return abs_num, abs
    return None, None


def choose_participant(data):
    app = QApplication(sys.argv)
    edit = QLineEdit()
    completer = QCompleter()
    edit.setCompleter(completer)

    model = QStringListModel()
    completer.setModel(model)
    model.setStringList([abs['main_author'].get('name') for abs in data['abstracts']])

    edit.returnPressed.connect(app.quit)
    edit.show()
    app.exec_()
    return edit.text()


def sanitize_fname(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    value = str(unicodedata.normalize('NFKD', value).encode('ascii', 'ignore'))
    value = re.sub('[^-\w\s.]', '', value).strip().lower()
    value = re.sub('[-\s]+', '-', value)
    value = re.sub('[.]+', '.', value)
    return value

def last_name(data):
    names = data['name'].split(' ')
    last_name = names[-1]
    if len(names) >= 2 and names[-2].lower().strip() in ['de', 'di', 'van', 'von']:
        last_name = names[-2] + ' ' + last_name
    if len(names) >= 3 and names[-3].lower().strip() in ['van', 'von']:
        last_name = names[-3] +' '+names[-2] + ' ' + last_name
    return last_name

def normalize(x):
    return unidecode.unidecode(x).replace(' ', '').strip().lower()

def deaccent(x):
    return unidecode.unidecode(x)

def process_abstract(eml, data, log=sys.stdout):

    from_name, from_email = email.utils.parseaddr(eml["from"])
    to_addr = email.utils.parseaddr(eml["to"])
    msg_id = eml['message-id']
    #date = email.utils.parsedate_to_datetime(eml["date"])
    subject = unquote(eml["subject"])

    abs_num, abs = get_abs_by_email(data, from_email)
    if not abs:
        abs_num, abs = get_abs_by_name(data, from_name)
    if not abs:
        abs_num, abs = get_abs_by_name(data, choose_participant(data))

    if not abs:
        return False

    slides = []
    if 'tutorial' in abs:
        sched = abs['schedule'][0]
    else:
        sched = abs['schedule']

    down_dir = Path(__file__).parent/'files'
    tgt_dir = Path(sched['day'])/sched['room'].replace(' ', '-')/normalize(last_name(abs['main_author']))
    (down_dir/tgt_dir).mkdir(parents=True, exist_ok=True)

    for part in eml.walk():
        if part.get_filename():
            tgt_fname = part.get_filename().replace(' ', '-')
            content = io.BytesIO(part.get_payload(decode=True)).getvalue()
            hash = hashlib.sha256(content).hexdigest()
            mime = part.get_content_type()
            (down_dir/tgt_dir/tgt_fname).write_bytes(content)
            slides.append({
                'path': str(tgt_dir/tgt_fname),
                'mime': mime,
                'hash': hash
            })
    abs['slides'] = slides
    data['abstracts'][abs_num] = abs
    print(slides, file=log)

    rsync(down_dir, log)

    base_url = "https://lc2019.cz/static/slides/"
    links = "\n    ".join([base_url + s['path'] + "?hash=" + s['hash'] for s in slides])

    body = f"""
Dear {deaccent(abs['main_author']['name'])},

thank you for sending your slides. We will upload them to the website.

Best,

Slides Robot
(on behalf of the organizers)
""".strip()

    args = ["kmail", "--subject", "Re: "+subject, "--body", body, "--identity", "Lc2019-slides", from_email]
    print("Running", "/usr/bin/kmail", args, file=log)
    os.spawnl(os.P_NOWAIT, '/usr/bin/kmail', *args)
    return abs, slides

def rsync(src_dir, log):
    args = ["rsync", "-O", "--archive", "--delete", str(src_dir)+"/", "verner@lc2019.cz:/data/www/workshop.math.cas.cz/lc2019.cz/static/slides/"]
    print("Syncing", "/usr/bin/rsync", ' '.join(args[1:]), file=log)
    os.spawnl(os.P_NOWAIT, '/usr/bin/rsync', *args)


if __name__ == "__main__":
    with open('/tmp/log', 'a') as log:
        try:
            print("Processing", sys.argv, file=log)
            print("Parsing email", file=log)
            eml = email.message_from_file(sys.stdin)
            print({
                'from': eml['from'],
                'id': eml['message-id'],
                'subj': eml['subject'],
                'date': eml['date']
            }, file=log)
            print("Loading sched data", file=log)
            ABS_DATA_FILE = Path(__file__).parent.parent/'data.json'
            data_json = json.loads(ABS_DATA_FILE.read_text())
            print("Processing mail", file=log)
            abs, slides = process_abstract(eml, data_json, log)
            SL_DATA_FILE = (Path(__file__).parent.parent/'slides.json')
            try:
                slides_json = json.loads(SL_DATA_FILE.read_text())
            except:
                slides_json ={}
            slides_json[abs['main_author']['name']] = slides
            SL_DATA_FILE.write_text(json.dumps(slides_json,indent=4))
            ABS_DATA_FILE.write_text(json.dumps(data_json, indent=4))

        except Exception as ex:
            traceback.print_exc(file=log)
