var layout = require('../../vendor/js/node_modules/image-layout/layouts/fixed-partition');
var galleryElt = document.getElementById('gallery');
var galleryInner = document.getElementById('gallery-inner');

var runLayout = function() {
    galleryInner.style.display='none'
    var result = layout(window.photos, {
        align: 'center',
        containerWidth: Math.floor(galleryElt.clientWidth),
        idealElementHeight: Math.floor(window.innerHeight/3),
        spacing: 10
    });
    window.targetWidth = Math.floor(galleryElt.clientWidth);
    window.result = result;
    galleryElt.style.height=result.height+'px'
    for(var i = 0, n = window.photos.length; i < n; i++) {
        var photo = window.photos[i], pos = result.positions[i]
        var img = document.getElementById(window.photos[i].filename)
        img.style.position='absolute'
        img.style.left=pos.x+'px'
        img.style.top=pos.y+'px'
        img.style.height=pos.height +'px'
        img.style.width=pos.width+'px'
        img.style.display='block'
    }
    galleryInner.style.display='block'
}

var layoutTask = undefined

var resizeHandler = function() {
    if (layoutTask) return
    layoutTask = window.setTimeout(function() {
        runLayout()
        layoutTask = undefined
    }, 100)
}

window.onresize = resizeHandler
runLayout()


