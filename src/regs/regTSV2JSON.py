#!/usr/bin/env python3

import csv
import json
import sys

from pathlib import Path

import unidecode
import requests


src_tsv = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTaY1sGFX3bS_4tacmRmO5oxGiydxnsRn1eMy9IRr6n1R4892dA8HIaBZ_vLbfh7io3SYR-6Q4gI_rw/pub?gid=1792087114&single=true&output=tsv"
r = requests.get(src_tsv)
lines = str(r.content, encoding='utf-8').split('\n')
src = csv.reader(lines, delimiter='\t', quotechar='"')

args = [a for a in sys.argv[1:] if not a.startswith('--')]
out_file = Path(args[0]) if len(args) > 0 else None


def letter2col(ch):
    return ord(ch.upper())-ord('A')


def show_in_list(row):
    if row[letter2col('W')].strip() == '0 Kč':
        return True
    if 'supp' in row[letter2col('X')]:
        return True
    if 'no-pay' in row[letter2col('X')]:
        return True
    return False

def sort_key(row):
    return unidecode.unidecode(row[letter2col('B')]+row[letter2col('A')]).replace(' ', '').strip().lower()

data = []
count = 0
for row in src:
    count += 1
    if count == 1:
        update_time = row[letter2col('C')]
    if count < 3:
        continue
    reg = {
      'first_name': row[letter2col('A')].strip(),
      'last_name': row[letter2col('B')].strip(),
      'institution': row[letter2col('C')].strip(),
      'sortkey': sort_key(row),
      'show': True
    }
    data.append(reg)

if out_file:
    out_data = json.loads(out_file.read_text())
    out_data.update({
        'registrations_last_update': update_time,
        'registrations': data,
    })
    out_file.write_text(json.dumps(out_data, indent=4))
else:
    print(json.dumps({'registrations': data}, indent=4))

