#!/bin/bash

OIFS="$IFS"
IFS=$'\n'
NUM=0
TAB=$'\t'
SMAZ=`echo -e "$SMAZ"`
for abs in `find . -name "*.tex"`; do
    echo "Processing $abs"
    AUTH_FILE=`basename "$abs" .tex`;
    PDF=`echo "$abs" | sed -e's/.tex$/.pdf/g'`
    if [ -f "$PDF" ]; then
	echo "Pdf: $PDF";
    else
	DIR_NAME=`dirname "$abs"`
	PDF=`find $DIR_NAME -name "*.pdf" | head -1`
	if [ -f "$PDF" ]; then
	    echo "Candidate Pdf: $PDF";
	else
	    PDF="missing";
	    echo "MISSING PDF";
	fi;
    fi;
    let NUM=NUM+1
    DOWNLOAD_PDF=$NUM-`basename "$PDF"| sed -e's/  */-/' | tr ' ' '-' | tr -d ','`
    echo "$DOWNLOAD_PDF$TAB$PDF" >> sections.tsv;
done
IFS="$OIFS"

