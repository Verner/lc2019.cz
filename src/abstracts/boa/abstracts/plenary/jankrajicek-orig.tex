
%% TO PROCESS THIS FILE YOU WILL NEED TO DOWNLOAD asl.cls from %% http://aslonline.org/abstractresources.html. 

\documentclass[bsl,meeting]{asl}
\AbstractsOn
\pagestyle{plain}
\def\urladdr#1{\endgraf\noindent{\it URL Address}: {\tt #1}.}
\newcommand{\NP}{}%\usepackage{verbatim}
\begin{document}
\thispagestyle{empty}

%% BEGIN INSERTING YOUR ABSTRACT DIRECTLY BELOW; %% SEE INSTRUCTIONS (1), (2), (3), and (4) FOR PROPER FORMATS\NP 
 
 \absauth{Jan Kraj\'{\i}\v{c}ek}
 \meettitle{Model theory and proof complexity}
 \affil{Faculty of Mathematics and Physics, Charles University, Sokolovsk\'{a} 83, Prague, Czech Republic}
 \meetemail{krajicek@karlin.mff.cuni.cz}

%% INSERT TEXT OF ABSTRACT DIRECTLY BELOW

 Mathematical logic and computational complexity theory
have many topics in common. In most cases the links between
the two fields are fostered by finite combinatorics, 
manifesting either via proof theory or via finite model theory.

  There are, however, also topics in complexity theory where 
infinitary methods of logic shed a new light on old problems.
I will discuss, in particular, how non-finite model theory 
relates to proof complexity. The relevant model theoretical 
problems involve constructions of models of bounded arithmetic
and of expanded extensions of pseudo-finite structures. I will 
describe forcing with random variables aimed at tackling 
these problems, and give some examples of results that can be 
obtained in this way.


\end{document}