#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests
import re

from pathlib import Path
from collections import defaultdict

from utils import normalize

import html


def texsched(data):
    return "{"+"}{".join([data[k] for k in ['day', 'room', 'start', 'stop']])+"}"

def texescape(s):
    return html.unescape(s).replace('&', '\\&')

src_data = json.loads(Path(sys.argv[1]).read_text())
data = defaultdict(list)

authors = []
for abs in src_data['abstracts']:
    if 'boa' in abs:
        data[abs['schedule']['session']].append(abs)


CITE_PAT = re.compile(r'\\cite([^{]*){([^}]*)}')
BIB_PAT = re.compile(r'\\bibitem{([^}]*)}')
REMOVE = [
    r'\\documentclass[^}]*}',
    r'\\(this)?pagestyle[^}]*}',
    #r'\\pagestyle[^}]*}',
    r'\\AbstractsOn',
    r'\\begin{document}',
    r'\\end{document}.*',
    r'\\newcommand{\\NP}{}',
    r'\\def\\urladdr[^\n]*',
    r'\\usepackage[^}]*}',
]
REMOVE_PAT = re.compile('|'.join(['('+pat+')' for pat in REMOVE]), re.DOTALL)

auth_index = defaultdict(list)

total_count = 0
out_dir = Path('boa') / 'abstracts'
for sect, abstracts in data.items():
    print("Constructing section", sect, end='')
    sect_id = sect.lower().replace(' ', '-').replace('unspecified', 'general')
    sect_dir = out_dir/sect_id
    if not sect_dir.exists():
        sect_dir.mkdir()
    sect_file = (sect_dir/'index.tex').open('w')
    abs_count = 0
    invited = [abs for abs in abstracts if abs['status'] == 'invited']
    contributed = [abs for abs in abstracts if abs['status'] != 'invited']
    for abstracts, scommand in [(invited, "\\startinvited"), (contributed, "\\startcontributed")]:
        abstracts.sort(key=lambda x: x['sort_key'])
        if sect_id != 'plenary':
            sect_file.write(scommand+"\n")

        for abs in abstracts:
            total_count += 1
            abs_count += 1
            src_tex = (out_dir/abs['boa']['orig']).read_text()
            out_path = (out_dir/abs['boa']['tex'])
            abs_id = normalize(abs['main_author'].get('fullname_reversed', abs['main_author']['name']))
            sched_data = abs['schedule']

            def citekeyrepl(matchobj):
                return "\\cite"+matchobj.group(1)+"{"+(','.join([abs_id+':'+k for k in matchobj.group(2).split(',')])).replace(' ','')+"}"

            def bibitemkeyrepl(matchobj):
                return "\\bibitem{"+','.join([abs_id+':'+k for k in matchobj.group(1).split(',')])+"}"

            if len(abs['auth']) > 1:
                auth_tex = ', '.join([a['name'] + ("*" if a.get('main', False) else '') for a in abs['auth']])
            else:
                auth_tex = abs['auth'][0]['name']

            title_tex = texescape(abs['title'])

            label = 'abs:'+abs_id
            schedule_tex = "\\schedule"+texsched(sched_data)

            for a in abs['auth']:
                names = a['name'].split(' ')
                reversed_name = names[-1]+', '+' '.join(names[:-1])
                auth_index[reversed_name].append({
                    'main': a.get('main', False),
                    'label': label,
                    'sched': sched_data
                })
            src_tex = re.sub(r'%.*$', '', src_tex)  # Throw away comments
            src_tex = re.sub(r"\\DeclareMathOperator{([^}]*)}{([^}]*)}", r'\\newcommand{\1}{\\hbox{\2}}', src_tex)
            src_tex = CITE_PAT.sub(citekeyrepl, src_tex)
            src_tex = BIB_PAT.sub(bibitemkeyrepl, src_tex)
            src_tex = REMOVE_PAT.sub('', src_tex)
            src_tex += "\n\\vfill\\eject"
            src_tex = """
    \\abstract{"""+title_tex+"""}{"""+auth_tex+"""}
    \\label{"""+label+"""}
    """ +   schedule_tex + src_tex
            out_path.write_text(src_tex)
            sect_file.write("\\input{abstracts/"+abs['boa']['tex']+"}\n")

    print("...", abs_count)
print("Total abstracts", total_count)

# Create the list of participants
reg_file = out_dir/'participants-list.tex'
parts_tex = []
participants = src_data['registrations']
participants.sort(key=lambda x: x['sortkey'])
for part in participants:
    if part['show']:
        parts_tex.append(texescape("\\participant{"+part['first_name']+"}{"+part['last_name']+"}{"+part.get('institution', "")+"}"))

reg_file.write_text("\n".join(parts_tex))

# Create index
ind_file = out_dir/'index-list.tex'
auth_keys = sorted(auth_index.keys(), key=lambda x: normalize(x))
auth_index_tex = []
for auth in auth_keys:
    talks = auth_index[auth]
    auth_index_tex.append("\\indexline{"+auth+"}{"+",".join([
        ("\\mainabsref{" if t['main'] else "\\absref{")+t['label']+"}"+texsched(t['sched']) for t in talks
    ])+"}")
ind_file.write_text("\n".join(auth_index_tex))
