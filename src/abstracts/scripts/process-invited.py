#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys

from pathlib import Path

from utils import get_registration, get_registrations, normalize

abs_src = csv.reader(Path('abstracts.tsv').open(), delimiter='\t', quotechar='"')

regs = get_registrations()

for row in abs_src:
    if 'invited' not in row[-1]:
        continue
    abs_name, path_name, title, down_pdf, abs_src = row

    data = {'fullname':abs_name.strip()}
    reg, score = get_registration(data, regs)
    if score >= 0.4:
        data['fullname'] = path_name
        reg, score = get_registration(data, regs)
    if score >= 0.4:
        name = abs_name
    else:
        name = reg['firstname']+' '+reg['lastname']

    if 'Sessions' in abs_src:
        section = Path(abs_src).parts[2]
    else:
        section = 'Plenary'
    status ='invited'
    eml = '???'
    all_authors = name
    statuses = 'paid'

    print('\t'.join([status, name, eml, all_authors, statuses, section, title, abs_src, down_pdf]))
