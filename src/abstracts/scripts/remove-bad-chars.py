#!/usr/bin/env python3

import sys
stdin = sys.stdin.buffer.read()
for c in stdin:
    if c < 32 or c > 128:
        continue
    sys.stdout.buffer.write(bytes([c]))
