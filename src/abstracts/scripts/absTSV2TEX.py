#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests
import re

from pathlib import Path
from collections import defaultdict

from utils import get_registration, get_registrations, normalize


src_tsv = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQZ97lPxiH37LYugBhYvhKwWgxb13PG1V2ZGKeFKO2GKoSYSVHASDzo_d5-u_UrU8YTrFfwnFO_teAj/pub?gid=1720766797&single=true&output=tsv'
r = requests.get(src_tsv)
lines = str(r.content, encoding='utf-8').split('\n')
src = csv.reader(lines, delimiter='\t', quotechar='"')

args = [a for a in sys.argv[1:] if not a.startswith('--')]
# src = csv.reader(Path(args[0]).open(), delimiter='\t', quotechar='"')

data = defaultdict(list)
regs = get_registrations(refresh='--refresh' in sys.argv)


data = json.loads(Path(sys.argv[1]).read_text())

count = 0
for row in src:
    count += 1
    if count == 1:
        continue
    item = {
      'status': row[0],
      'auth': [{'name': a.strip()} for a in row[3].strip().replace(' and ', ',').split(',') if a.strip()],
      'title': row[6],
      'path': row[8],
      'section': row[5],
      'src': row[7]
    }
    if item['status'] not in ['paid', 'invited']:
        continue

    # Guess main author
    main_author = {'name': row[1], 'status': row[0], 'email': row[2]}
    reg, score = get_registration(main_author, regs)
    if reg and score < 0.4:
        main_author.update(reg)

    sort_keys = []
    for a in item['auth']:
        reg, score = get_registration(a, regs)
        if reg and score < 0.4:
            a.update(reg)
            a['score'] = score
            if reg['reg_id'] == main_author.get('reg_id', None):
                a['main'] = True
        else:
            names = a['name'].split(' ')
            a['fullname_reversed'] = ' '.join([names[-1]]+names[:-1])
            a['fullname'] = a['name']
            if a['fullname'] == main_author['name'] or a['fullname_reversed'] == main_author['name']:
                a['main'] = True
        sort_keys.append(normalize(a.get('fullname_reversed', a['fullname'])))

    if len(item['auth']) == 1:
        item['auth'][0]['main'] = True

    sort_keys.sort()
    item['sort_key'] = '-'.join(sort_keys)
    item['main_author'] = main_author

    data[item['section']].append(item)

CITE_PAT = re.compile(r'\\cite{([^}]*)}')
BIB_PAT = re.compile(r'\\bibitem{([^}]*)}')
REMOVE = [
    r'\\documentclass[^}]*}',
    r'\\(this)?pagestyle[^}]*}',
    #r'\\pagestyle[^}]*}',
    r'\\AbstractsOn',
    r'\\begin{document}',
    r'\\end{document}.*',
    r'\\newcommand{\\NP}{}',
    r'\\def\\urladdr[^\n]*',
    r'\\usepackage[^}]*}',
]
REMOVE_PAT = re.compile('|'.join(['('+pat+')' for pat in REMOVE]), re.DOTALL)

for sect, abstracts in data.items():
    print("Constructing section", sect)
    sect_id = sect.lower().replace(' ', '-')
    sect_dir = Path('boa')/'abstracts'/sect_id
    if not sect_dir.exists():
        sect_dir.mkdir()
    sect_file = (sect_dir/'index.tex').open('w')
    sect_file.write("""
\\chapter{"""+sect+"""}
\\vfill
\\eject
""")

    abstracts.sort(key=lambda x: x['sort_key'])
    for abs in abstracts:
        src_tex = Path(abs['src']).read_text()
        abs_id = normalize(abs['main_author'].get('fullname_reversed', abs['main_author']['name']))
        out_path = sect_dir/(abs_id+'.tex')
        source_path = sect_dir/(abs_id+'-orig.tex')
        source_path.write_text(src_tex)

        def citekeyrepl(matchobj):
            return "\\cite{"+','.join([abs_id+':'+k for k in matchobj.group(1).split(',')])+"}"

        def bibitemkeyrepl(matchobj):
            return "\\bibitem{"+','.join([abs_id+':'+k for k in matchobj.group(1).split(',')])+"}"

        if len(abs['auth']) > 1:
            auth_tex = ', '.join([a['name'] + ("*" if a.get('main', False) else '') for a in abs['auth']])
        else:
            auth_tex = abs['auth'][0]['name']

        title_tex = abs['title'].replace('&', '\\&')

        labels_tex = '\n'.join(['\\label{abs:'+normalize(a['name'])+'}' for a in abs['auth']])

        src_tex = re.sub(r'%.*$', '', src_tex)  # Throw away comments
        src_tex = re.sub(r"\\DeclareMathOperator{([^}]*)}{([^}]*)}", r'\\newcommand{\1}{\\hbox{\2}}', src_tex)
        src_tex = CITE_PAT.sub(citekeyrepl, src_tex)
        src_tex = BIB_PAT.sub(bibitemkeyrepl, src_tex)
        src_tex = REMOVE_PAT.sub('', src_tex)
        src_tex += "\n\\vfill\\eject"
        src_tex = """
\\abstract{"""+title_tex+"""}{"""+auth_tex+"""}
""" + labels_tex + "\n" + src_tex
        out_path.write_text(src_tex)
        sect_file.write("\\input{abstracts/"+sect_id+"/"+abs_id+"}\n")
