#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests

from pathlib import Path

from utils import get_registration, get_registrations, normalize


src_tsv = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQZ97lPxiH37LYugBhYvhKwWgxb13PG1V2ZGKeFKO2GKoSYSVHASDzo_d5-u_UrU8YTrFfwnFO_teAj/pub?gid=1720766797&single=true&output=tsv'
r = requests.get(src_tsv)
lines = str(r.content, encoding='utf-8').split('\n')
src = csv.reader(lines, delimiter='\t', quotechar='"')

args = [a for a in sys.argv[1:] if not a.startswith('--')]
# src = csv.reader(Path(args[0]).open(), delimiter='\t', quotechar='"')
out_file = Path(args[0]) if len(args) > 0 else None

data = []
regs = get_registrations(refresh='--refresh' in sys.argv)

count = 0
for row in src:
    count += 1
    if count == 1:
        continue
    item = {
      'status': row[0],
      'auth': [{'name': a.strip()} for a in row[3].strip().replace(' and ', ',').split(',') if a.strip()],
      'title': row[6],
      'path': row[8],
      'section': row[5]
    }

    # Guess main author
    main_author = {'name': row[1], 'status': row[0], 'email': row[2]}
    reg, score = get_registration(main_author, regs)
    if reg and score < 0.4:
        main_author.update(reg)

    sort_keys = []
    for a in item['auth']:
        reg, score = get_registration(a, regs)
        if reg and score < 0.4:
            a.update(reg)
            a['score'] = score
            if reg['reg_id'] == main_author.get('reg_id', None):
                a['main'] = True
                a.update(main_author)
        else:
            names = a['name'].split(' ')
            a['fullname_reversed'] = ' '.join([names[-1]]+names[:-1])
            a['fullname'] = a['name']
            if a['fullname'] == main_author['name'] or a['fullname_reversed'] == main_author['name']:
                a['main'] = True
        sort_keys.append(normalize(a['fullname_reversed']))

    if len(item['auth']) == 1:
        item['auth'][0]['main'] = True

    sort_keys.sort()
    item['sort_key'] = '-'.join(sort_keys)
    item['main_author'] = main_author
    data.append(item)

sessions = [
    'Set Theory',
    'Model Theory',
    'Reflection Principles and Modal Logic',
    'Proof Theory and Proof Complexity',
    'Computability',
    'Foundations of Geometry',
    'General'
]

if out_file:
    out_data = json.loads(out_file.read_text())
    out_data.update({
        'abstracts': data,
        'sessions': sessions
    })
    out_file.write_text(json.dumps(out_data, indent=4))
else:
    print(json.dumps({
                        'abstracts': data,
                        'sessions': sessions
                    }, indent=4))
