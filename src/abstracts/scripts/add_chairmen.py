#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests

from collections import defaultdict
from pathlib import Path

from utils import normalize


src_tsv = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vR8ByyzRczXWY0XCvikep54xHM5H_XWGKNegP5okOAaxKsiQvab_uWhwuLIDkik_r7_W-8ACp0n39Yz/pub?gid=1858000465&single=true&output=tsv'
r = requests.get(src_tsv)
lines = str(r.content, encoding='utf-8').split('\n')
src = csv.reader(lines, delimiter='\t', quotechar='"')

data = json.loads(Path(sys.argv[1]).read_text())
out_file = Path(sys.argv[1])


chairmen = defaultdict(lambda : defaultdict(list))

ln = 0
for chair_row in src:
    if ln == 0:
        ln += 1
        continue
    try:
        day = chair_row[0].split(':')[0]
        for i in range(6):
            chairmen[day]['room-'+str(i)].append(chair_row[i+1])
    except:
        pass

data['chairmen'] = chairmen

out_file.write_text(json.dumps(data, indent=4))





