#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys

from pathlib import Path

from utils import get_registration, get_registrations, normalize

SECTIONS = {
    'Computability': 'Computability Session',
    'Model-Theory': 'Model Theory Session',
    'foundations-geom': 'Foundations of Geometry Session',
    'proof-theory': 'Proof Theory and Proof Complexity',
    'reflection-principles': 'Reflection Principles and Modal Logic Session',
    'set-theory': 'Set Theory Session'
}

sects = csv.reader(Path('sections.tsv').open(), delimiter='\t')
abs2sect = {}
for row in sects:
    for s in SECTIONS.keys():
        if s in row[1]:
            abs2sect[row[0]] = SECTIONS[s]
            break

args = [a for a in sys.argv[1:] if not a.startswith('--')]
src = csv.reader(Path(args[0]).open(), delimiter='\t', quotechar='"')

data = []

regs = get_registrations(refresh='--refresh' in sys.argv)


def normalize_status(stat):
    if 'podpora' in stat:
        return 'paid'
    if 'zapla' in stat:
        return 'paid'
    if 'neregistr' in stat:
        return 'waiting'
    if 'registr' in stat:
        return 'registered'
    return 'waiting'

mail_src = csv.reader(Path('mails.csv').open(), delimiter='\t', quotechar='"')
mail_db = {}
for person in mail_src:
    p = {
        'name': person[0]+' '+person[1]
    }
    reg, score = get_registration(p, regs)
    if reg and score < 0.4:
        reg['email'] = person[2]

    mail_db[normalize(p['name'])] = person[2]

mail_src = csv.reader(Path('david-sessions-nahouby.tsv').open(), delimiter='\t', quotechar='"')
auth2section = {}
header = {0: '', 1: 'Set Theory', 2: 'Model Theory', 3: 'Reflection Principles and Modal logic', 4: 'Proof Theory and Proof Complexity', 5: 'Computability', 6: 'Foundations of Geometry', 7: 'not specified'}
for row in mail_src:
    for ncol, col in enumerate(row):
        auth2section[col] = header[ncol]

count = 0
for row in src:
    count += 1
    if count == 1:
        continue
    abs2sect.get(row[3], 'unspecified')
    author_status = [normalize_status(s.strip()) for s in row[0].split(',')]
    item = {
      'status': normalize_status(row[0]),
      'auth': [{'name': a.strip()} for a in row[1].strip().replace(' and ', ',').split(',') if a.strip()],
      'title': row[2],
      'path': row[3],
      'section': abs2sect.get(row[3], 'Unspecified')
    }
    for i in range(min(len(author_status), len(item['auth']))):
        item['auth'][i]['status'] = author_status[i]

    sort_keys = []
    for a in item['auth']:
        reg, score = get_registration(a, regs)
        if reg and score < 0.4:
            a.update(reg)
            a['score'] = score
        else:
            names = a['name'].split(' ')
            a['fullname_reversed'] = ' '.join([names[-1]]+names[:-1])
            a['fullname'] = a['name']
            if normalize(a['name']) in mail_db:
                a['email'] = mail_db[normalize(a['name'])]
        sort_keys.append(a['fullname_reversed'])

    # Guess main author
    main_author = None
    if len(item['auth']) == 1:
        main_author = item['auth'][0]
    else:
        for part in Path(row[4]).parts:
            part = normalize(str(part).strip().replace('_', '-').split('-')[0])
            for a in item['auth']:
                if part in normalize(a.get('fullname', a['name'])):
                    a['main'] = True
                    main_author = a
                    break
                a['main'] = False
            if main_author:
                break
    if main_author:
        item['main_author'] = main_author
        item['status'] = 'paid' if 'zaplatil' in main_author.get('status', '') else 'waiting'
    else:
        item['main_author'] = "unknown"
    item['original_source'] = row[4].strip()
    sort_keys.sort()
    item['sort_key'] = '-'.join(sort_keys)
    data.append(item)


def save2db():
    conn = sqlite3.connect("lc2019.db", isolation_level=None)
    conn.row_factory = sqlite3.Row

    # Create schema


#print(json.dumps({'abstracts': data}, indent=4))
#print("")
print("\t".join(["Status", "Main Author", "Main Author Email", "All Authors", "Author Statuses", "Title", "TeX file"]))
for item in data:
    print("\t".join([
        item['status'],
        item['main_author']['fullname'] if item['main_author'] != 'unknown' else '?',
        item['main_author'].get('email', '?') if item['main_author'] != 'unknown' else '?'
        ','.join([a['fullname'] for a in item['auth']]),
        ','.join([a.get('status','') for a in item['auth']]),
        item['title'],
        item['original_source']
    ]))


