#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests
import re

from pathlib import Path
from collections import defaultdict

from utils import normalize

data_file = Path(sys.argv[1])

data = json.loads(data_file.read_text())

out_dir = Path('boa') / 'abstracts'
for abs in data['abstracts']:
    if 'schedule' in abs:
        sess = abs['schedule']['session']
        sess_id = sess.lower().replace(' ', '-').replace('unspecified', 'general')
        orig_sess_id = abs['section'].lower().replace(' ', '-')
        abs_id = normalize(abs['main_author'].get('fullname_reversed', abs['main_author']['name']))
        abs['boa'] = {
            'tex': sess_id + '/' + abs_id + '.tex',
            'orig': sess_id + '/' + abs_id + '-orig.tex'
        }
        if not (out_dir/abs['boa']['orig']).exists():
            orig_file = (out_dir / orig_sess_id / (abs_id+'-orig.tex')).read_text()
            (out_dir/abs['boa']['orig']).write_text(orig_file)

data_file.write_text(json.dumps(data, indent=4))
