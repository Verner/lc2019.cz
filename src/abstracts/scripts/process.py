#!/usr/bin/env python3
import email
import json
import io
import pickle
import subprocess
import sys
import tempfile

import jinja2
import lxml.html as l
import requests
import pdfminer.high_level
import spacy

from collections import defaultdict
from email.utils import unquote
from pathlib import Path

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request



SPREAD_SHEET_ID="1EDnMQesqtmKYLZsHqxwc95Pjvz9Tst7EeZ61KnNPzVw"
SPREAD_SHEET_RANGE="Sheet1!A:H"
REGISTRATION_URL = "https://system.amca.cz/index.php/registration/lc-2019/list_users"
EMAIL_TEMPLATE = ""
REGISTRATION_FILE = Path("/tmp/registrations.json")
CONVERT_DOC = "soffice --headless --convert-to txt:Text {doc_path}"
MODEL = "xx_ent_wiki_sm"
SESSIONS = ['proof theory', 'philosophy of mathematics', 'set theory', 'model theory', 'complexity', 'foundations of geometry']
UPLOAD_DIR = "verner@lc2019:/data/www/workshop.math.cas.cz/lc2019.cz/static/_private/abstracts/"
# MODEL = "en_core_web_sm"

def detex(content):
    with subprocess.Popen('/usr/bin/detex', stdout=subprocess.PIPE, stdin=subprocess.PIPE) as proc:
        proc.stdin.write(bytes(content, encoding="utf-8"))
        proc.stdin.close()
        return str(proc.stdout.read(), encoding='utf-8')

def get_txt_content(message_part):
    tp = message_part.get_content_type()
    if tp == 'text/plain':
        content = str(message_part.get_payload(decode=True), encoding=message_part.get_charset() or 'utf-8')
        if message_part.get_filename() and message_part.get_filename().endswith('.tex'):
            ret = detex(content)
            if ret:
                return ret
        return content
    elif tp == 'application/pdf':
        content = io.BytesIO(message_part.get_payload(decode=True))
        out_text = io.BytesIO()
        pdfminer.high_level.extract_text_to_fp(content, out_text, strip_control=True)
        return str(out_text.getvalue(), encoding='utf-8')
    return None


def get_registrations(refresh=False):
    if REGISTRATION_FILE.exists() and not refresh:
        return json.load(REGISTRATION_FILE.open())
    r = requests.get(REGISTRATION_URL)
    doc = l.document_fromstring(r.text)
    ret = []
    for row in doc.cssselect('table.user_list')[0].cssselect('tbody')[0].cssselect('tr'):
        user = defaultdict(None)
        cols = row.cssselect('td')
        user['reg_id'], user['firstname'], user['lastname'], user['institution'], user['city'] = [c.text for c in cols]
        user['fullname'] = user['firstname']+' '+user['lastname']
        user['fullname_reversed'] = user['lastname']+' '+user['firstname']
        ret.append(user)
    REGISTRATION_FILE.write_text(json.dumps(ret))
    return ret

DEACCENT = str.maketrans("ěščřžýáíéůú", "escrzyaieuu")
def normalize(x):
    return x.translate(DEACCENT).lower()

def distance(data, reg):
    if 'email' in data and 'email' in reg and data['email'] == reg['email'].lower():
        return 0
    name = normalize(data.get('name', ''))
    if name == normalize(reg['fullname']) or name == normalize(reg['fullname_reversed']):
        return 0

    names = name.split(' ')
    matches = 1
    for n in names:
        if normalize(reg['firstname']) == n or normalize(reg['lastname']) == n:
            matches += 1
    return (1/matches)

def section_distance(subj, sect):
    if sect in subj:
        return 0
    words = subj.lower().split(' ')
    matches = 1
    for w in words:
        if w in sect:
            matches += 1
    return (1/matches)

def get_registration(data, regs):
    candidate = (None, 1)
    for r in regs:
        d = distance(data, r)
        if d == 0:
            return (r, 0)
        if d < candidate[1]:
            candidate = (r, d)
    return candidate

#Person = namedtuple('Person', ['name', 'email', 'registration_id', "institution", 'city'])


#with open("/tmp/abstracts-proc","w") as OUT:
    #from_addr = email.utils.parseaddr(eml["from"])
    #to_addr = email.utils.parseaddr(eml["to"])
    #msg_id = eml['message-id']
    #date = email.utils.parsedate_to_datetime(eml["date"])
    #print("ID:", msg_id)
    #print("Date:", str(date))
    #print("From:", from_addr)
    #print("To:", to_addr)
    #print("Subj:", unquote(eml["subject"]))
    #print("Attachments:")
    #for part in eml.walk():
        #body = get_txt_content(part)
        #if not part.get_filename():
            #pass
        #print("--", part.get_filename(), "(", part.get_content_type(), ")")

        #if body:
            #print("--Body:")
            #print(body)
            #print("--People:")
            #doc = nlp(body)
            #for entity in [ent.text for ent in doc.ents if ent.label_ == 'PER']:
                #print(entity, end="; ")
            #print()


def upload_abstract(body, tp, id):
    if tp == 'tex':
        return ('ok', 'https://lc2019.cz/_private/submitted_abstracts/abs1.pdf')

def compile_tex(body):
    tmp_dir = Path(tempfile.mkdtemp())
    tmp_file = (tmp_dir/'abstract.tex')
    tmp_file.write_text(body)
    (tmp_dir/'asl.cls').write_text(Path('asl.cls').read_text())
    proc = subprocess.Popen(['latexmk', '-xelatex', str(tmp_file)])
    if proc.wait() != 0:
        return None
    return tmp_file.replace('.tex','.pdf')

def google_sheets_api():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if Path('token.pickle').exists():
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    return service.spreadsheets()

def add_row_to_google(api, row):

    result = api.values().append(
        spreadsheetId=SPREAD_SHEET_ID,
        range=SAMPLE_RANGE_NAME,
        body={'values':row},
        valueInputOption='USER_ENTERED',
        ).execute()

def process_abstract(eml, regs):
    from_name, from_email = email.utils.parseaddr(eml["from"])
    to_addr = email.utils.parseaddr(eml["to"])
    msg_id = eml['message-id']
    date = email.utils.parsedate_to_datetime(eml["date"])
    subject = unquote(eml["subject"])
    sections = [(section_distance(subject, sect), sect) for sect in SESSIONS]
    section_candidates = [sect for (dist, sect) in sections if dist < 0.5]

    data = {
        'name': from_name,
        'email': from_email
    }

    reg, score = get_registration(data, regs)
    abstracts = []

    for part in eml.walk():
        body = get_txt_content(part)
        print("SCORE:", score)
        print("--", part.get_filename())
        print(body)
        print("--")
        if score > 0 and body:
            doc = nlp(body)
            for entity in [ent.text for ent in doc.ents if ent.label_ == 'PER']:
                data['name'] = entity
                print("Finding", entity)
                r, s = get_registration(data, regs)
                if s < score:
                    reg = r
                    score = s

        if part.get_filename():
            if part.get_content_type() == 'application/pdf':
                content = io.BytesIO(part.get_payload(decode=True))
                abstracts.append(upload_abstract(str(content.getvalue()), tp='pdf'))
            elif part.get_filename().endswith('.tex'):
                abstracts.append(upload_abstract(str(part.get_payload(decode=True), encoding='utf-8'), tp='tex'))

    for status, link in abstracts:
        if status != 'ok':
            subprocess.Popen(["kile", link])
        else:
            subprocess.Popen(["okular", link.replace(".tex", ".pdf")])

    if reg:
        row = reg['fullname'], from_email, reg['reg_id'], date, ';'.join(section_candidates), ';'.join([str(l) for (_, l) in abstracts]), '', ''
    else:
        row = from_name, from_email, None, date, ';'.join(section_candidates), ';'.join([str(l) for (_, l) in abstracts]), '', ''

    print(*row)
    subprocess.Popen(["kmail", "-s", "Re: "+subject, "--body", "ahoj", from_email])


regs = get_registrations()
#eml = email.message_from_file(sys.stdin)
#nlp = spacy.load(MODEL)

#process_abstract(eml, regs)

for ln in Path('abstracts.tsv').open.readlines()[1:]:
    auth, title, abstract = ln.split("\t")
    authors = []
    tmp = auth.split(',')
    for a in tmp:
        authors.extend(a.split(' and '))
    data = {}
    regs = []
    for a in authors:
        data['fullname'] = a.strip()
        reg, score = get_registration(data)
        regs.append(reg)

