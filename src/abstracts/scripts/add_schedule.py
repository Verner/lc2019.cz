#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys
import requests

from collections import defaultdict
from pathlib import Path

from utils import normalize


src_tsv = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vR8ByyzRczXWY0XCvikep54xHM5H_XWGKNegP5okOAaxKsiQvab_uWhwuLIDkik_r7_W-8ACp0n39Yz/pub?gid=236048539&single=true&output=tsv'
r = requests.get(src_tsv)
lines = str(r.content, encoding='utf-8').split('\n')
src = csv.reader(lines, delimiter='\t', quotechar='"')

data = json.loads(Path(sys.argv[1]).read_text())
out_file = Path(sys.argv[1])

sched = defaultdict(list)

for sched_row in src:
    try:
        speaker, all_auths, session, session_cat, notes, scheduled, day, time, room, title = sched_row
        start, stop = time.split('–')
        room = (room.split('–')[0]).strip()
        sched[title.strip()].append({
            'day': day.strip(),
            'session': session.strip(),
            'room': room,
            'start': start,
            'stop': stop,
        })
    except:
        pass

for abs in data['abstracts']:
    ttl = abs['title'].strip()
    s = sched[ttl]
    if s:
        if len(s) == 1:
            abs['schedule'] = s[0]
        else:
            abs['tutorial'] = True
            abs['schedule'] = s
    elif abs['status'] in ['invited', 'paid'] and abs['section'] != 'Plenary':
        print("Talk", abs['title'], "by", abs['main_author']['name'], "not scheduled")

out_file.write_text(json.dumps(data, indent=4))




