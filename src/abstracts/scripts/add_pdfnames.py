#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys

from pathlib import Path

from utils import get_registration, get_registrations, normalize

abs_src = csv.reader(Path('abstracts.tsv').open(), delimiter='\t', quotechar='"')
pth2name = {}
for row in abs_src:
    pth, name = row[-1], row[-2]
    pth2name[pth] = name

args = [a for a in sys.argv[1:] if not a.startswith('--')]
src = csv.reader(Path(args[0]).open(), delimiter='\t', quotechar='"')

count = 0
for row in src:
    count += 1
    if count == 1:
        print("\t".join(row+['PDF']))
        continue
    pth = row[7]
    row.append(pth2name.get(pth, '???'))
    print("\t".join(row))



