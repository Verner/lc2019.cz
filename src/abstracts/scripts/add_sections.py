#!/usr/bin/env python3

import csv
import json
import sqlite3
import sys

from pathlib import Path

from utils import get_registration, get_registrations, normalize

sects_src = csv.reader(Path('david-sessions-nahouby.tsv').open(), delimiter='\t', quotechar='"')
auth2section = {}
header = {0: '', 1: 'Set Theory', 2: 'Model Theory', 3: 'Reflection Principles and Modal logic', 4: 'Proof Theory and Proof Complexity', 5: 'Computability', 6: 'Foundations of Geometry', 7: 'General'}
for row in sects_src:
    for ncol, col in enumerate(row):
        auth_name = normalize(col)
        auth2section[auth_name] = header[ncol]

args = [a for a in sys.argv[1:] if not a.startswith('--')]
src = csv.reader(Path(args[0]).open(), delimiter='\t', quotechar='"')

count = 0
for row in src:
    count += 1
    if count == 1:
        print("\t".join(row))
        continue
    auth_names = row[1].split(' ')
    section = 'General'
    for n in [normalize(n) for n in row[1].split(' ')]:
        if n in auth2section:
            section = auth2section[n]
    row[5] = section
    print("\t".join(row))


