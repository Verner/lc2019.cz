#!/bin/bash

dec2hex(){
  echo -e "ibase=10\nobase=16\n$1" | bc
}
OIFS="$IFS"
IFS=$'\n'
EXTRACT_ARGUMENT=' *{\([^}]*\)}.*/\1/g'
NUM=0
TAB=$'\t'
SMAZ=""
for i in `seq 1 31`; do
    HEX=$(dec2hex $i);
    SMAZ="$SMAZ\x$HEX"
done
SMAZ=`echo -e "$SMAZ"`
for abs in `find . -name "*.tex" | grep -v boa | grep -v 'book_of'`; do
    echo "Processing $abs"
    CONTENT=`cat "$abs" | tr -d '\r' | tr '\n' ' ' |  iconv --from utf-8 --to ascii -c | ./scripts/remove-bad-chars.py | sed -e's/author/absauth/g'`
    if echo "$CONTENT" | grep --silent title; then
    	TITLE=`echo "$CONTENT" | sed -e's/{[^}]*Title[^}]*}//g' | sed -e's/.*title'$EXTRACT_ARGUMENT`
    else
	TITLE='Unknown';
    fi;
    if echo "$CONTENT" | grep --silent absauth; then
        AUTH=`echo "$CONTENT" | sed -e's/{[^}]*FirstName[^}]*}//g' | sed -e's/.*absauth'$EXTRACT_ARGUMENT`
    else
	AUTH="Unknown";
    fi;
    AUTH_FILE=`basename "$abs" .tex`;
    PDF=`echo "$abs" | sed -e's/.tex$/.pdf/g'`
    echo "Title: $TITLE"
    echo "Author: $AUTH"
    echo "Author from file: $AUTH_FILE"
    if [ -f "$PDF" ]; then
	echo "Pdf: $PDF";
	TSV_LINE="$TSV_LINE\t$PDF";
    else
	DIR_NAME=`dirname "$abs"`
	PDF=`find $DIR_NAME -name "*.pdf" | head -1`
	if [ -f "$PDF" ]; then
	    echo "Candidate Pdf: $PDF";
	else
	    PDF="missing";
	    echo "MISSING PDF";
	fi;
    fi;
    let NUM=NUM+1
    DOWNLOAD_PDF=$NUM-`basename "$PDF"| sed -e's/  */-/' | tr ' ' '-' | tr -d ','`
    if [ -f "$PDF" ]; then
    	cp "$PDF" downloads/$DOWNLOAD_PDF;
    fi;
    echo "$AUTH$TAB$AUTH_FILE$TAB$TITLE$TAB$DOWNLOAD_PDF$TAB$abs" >> abstracts.tsv;
done
IFS="$OIFS"

