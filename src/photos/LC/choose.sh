#!/bin/bash
for i in `ls *jpg | sort -t'-' --key=2,2 --key=3,3n`; do 
    if [ -f choice/$i ]; then 
	echo '  {"filename":"'$i'", "width":'`file $i | cut -d',' -f 15 | cut -d'x' -f1 | tr -d ' '`', "height":'`file $i | cut -d',' -f 15 | cut -d'x' -f2`'},'; 
    fi; 
done > choice.json
