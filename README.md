# Prerequisites

To build the website you will need

  - the `ruby-sass` [SASS](https://sass-lang.com/ruby-sass) compiler
  - the [`cssmin`](https://github.com/zacharyvoase/cssmin) CSS minifier
  - [`postcss`](https://postcss.org/) CSS postprocessor
  - the Python3 version of the [Jinja2](http://jinja.pocoo.org/docs/2.10/) templating library
  - the [`rsync`](https://rsync.samba.org/) program
  - the [`make`](https://www.gnu.org/software/make/) program
  - a ssh client

To install these on a debian/ubuntu system run the following in a shell


```bash
       $ sudo apt install python3-jinja2 ruby-sass python3-cssmin rsync make nodejs openssh-client
       $ npm install -g postcss
```

and add `$HOME/.npm-packages/bin` to your path

# Building & Updating

Building the website, uploading it to the server & running it locally is done
using the `Makefile`. The relevant targets are `all` to build, `deploy` to upload
it to the live server [www.lc2019.cz](https://www.lc2019.cz) and `serve` to run a local webserver
serving the website at [http://localhost:8000](http://localhost:8000), e.g. to
deploy run the following in the root directory of your git repo:

```bash
       $ make deploy
```

# Source Layout

## Styling
The styles are located in `src/style`. The main file there is `core.scss` which includes other files. The styles are written
in the [SCSS language]([SASS](https://sass-lang.com/ruby-sass)) (an extension of css).

## Building the html files
The html for the pages is compiled from Jinja2 templates located in `src/site` using the script `compile-templates.py` which
is called automatically by `make`. Each file in the directory corresponds to a single
webpage on the site, e.g. `src/site/venue.html` corresponds to `https://www.lc2019.cz/venue`. The files are Jinja2 html
templates. For documentation of the template language see [here](http://jinja.pocoo.org/docs/2.10/). The files can
reference other templates in the `src/templates` subdirectory (e.g. the skeleton of the webpage containing the header, footer, style references, etc.
is located in `src/templates/base.html`).

Images and other media are located in `src/media/core`.

To build the website, run `make`, which will build the final website in the subdirectory `build`. To see how the website will look like
you can run `make serve` and open [http://localhost:8000](http://localhost:8000) in your webbrowser.

To deploy the website to the live server at [https://lc2019.cz](https://lc2019.cz) first build it (running `make`) and then
run `make deploy`.

# Server setup

To add a new developer with username, e.g., `hacker` run the following on the server

```bash
       $ sudo adduser hacker
       $ sudo -u hacker mkdir ~hacker/.ssh
       $ sudo -u hacker cp hacker_id_rsa.pub ~hacker/authorized_keys
       $ sudo chmod -R og-rwx ~hacker/.ssh
       $ sudo -u hacker ln -s /home/www/lc2019.cz-static ~/web-lc2019
       $ sudo setfacl -md'u:hacker:rwx' /home/www/lc2019.cz-static
       $ sudo setfacl -m'u:hacker:rwx' /home/www/lc2019.cz-static
```
