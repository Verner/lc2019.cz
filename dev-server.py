#!/usr/bin/env python3

import os
import sys

from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer


class RequestHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        possible_name = self.path.strip("/")+'.html'
        if self.path == '/':
            # default routing, instead of "index.html"
            self.path = '/index.html'
        elif os.path.isfile(possible_name):
            self.path = possible_name

        return SimpleHTTPRequestHandler.do_GET(self)

port = 8000
if len(sys.argv) > 1:
    try:
        p = int(sys.argv[1])
        port = p
    except ValueError:
        print("port value provided must be an integer")

print("serving on port {port}".format(port=port))
server = TCPServer(('0.0.0.0', port), RequestHandler)
server.serve_forever()
