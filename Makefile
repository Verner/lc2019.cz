
beta_deploy_dir = /home/www/lc2019.cz-static/
deploy_dir = /data/www/workshop.math.cas.cz/lc2019.cz/

all: style html media hashes

style: structure build/static/core/css/core.css build/static/core/css/materialize.css
html: structure  build/index.html build/accommodation.html build/venue.html build/clmpst.html build/contact.html build/program.html build/registration.html build/support.html build/abstracts.html build/participants.html build/schedule.html build/social.html build/old_program.html build/invited.html build/restaurants.html build/admin_sched.html build/photos.html build/slides.html
hashes: build/static/hashes

structure:
	mkdir -p build/static/core/css
	mkdir -p build/static/core/img
	mkdir -p build/static/core/fonts
	mkdir -p build/static/vendor/js
	mkdir -p build/static/abstracts
	mkdir -p build/static/slides
	mkdir -p build/static/public/slides

build/static/core/css/core.css: src/style/*.scss
	sass --scss src/style/core.scss | cssmin | postcss --no-map -u autoprefixer > $@

build/static/hashes: style src/media/core/*/* src/abstracts/downloads/* src/slides/files/* src/slides/files/*/*/*/*
	sha512sum build/static/core/*/* > $@
	sha512sum build/static/abstracts/* >> $@
	sha512sum build/static/public/slides/*/*/*/* >> $@
	sha512sum build/static/public/slides/*.zip  >> $@

build/static/core/css/materialize.css: src/style/materialize-css/sass/components/*.scss
	sass --scss src/style/materialize-css/sass/materialize.scss | cssmin | postcss --no-map -u autoprefixer > $@

build/static/vendor/js/%.js: src/media/vendor/js/%.js
	cp src/media/vendor/js/`basename $<` $@

build/%.html: src/site/%.html src/templates/* src/data.json hashes
	./compile-template.py --context ./src/data.json --hashes ./build/static/hashes `basename $<` $@

media: structure src/media/core/fonts/* src/media/core/img/*
	rsync -O --archive --delete --exclude css src/media/core/ build/static/core/
	rsync -O --archive --delete src/abstracts/downloads/ build/static/abstracts/
	rsync -O --archive --delete src/slides/files/ build/static/public/slides/
	rsync -O --archive --delete src/photos/LC/ build/static/photos/
	rsync -O --archive --delete src/abstracts/boa/boa.pdf build/static/abstracts/LC2019-Book_of_abstracts.pdf
	webpack src/media/core/js/gallery.js -o build/static/core/js/gallery.js

deploy: all
	rsync -O  --archive --delete build/ verner@lc2019.cz:$(deploy_dir)

deploy_beta: all
	rsync -O  --archive --delete build/ beta.lc2019.cz:$(beta_deploy_dir)

serve: all
	cd build; ../dev-server.py

install:
	@echo On Debian/Ubuntu systems run the following
	@echo
	@echo    sudo apt install python3-jinja2 ruby-sass python3-cssmin
	@echo    npm install -g postcss
	@echo
	@echo and put ~/.npm-packages/bin into your path
