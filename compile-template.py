#!/usr/bin/env python3

import argparse
import jinja2
import json
import sys

from pathlib import Path

tpl_loader = jinja2.FileSystemLoader(['./src/templates','./src/site'])
tpl_env = jinja2.Environment(loader=tpl_loader)


@jinja2.contextfunction
def icon(icon_name):
    """
        {{ fa_icon("address-book") }} renders as <span class='icon fa fa-address-book'></span>
    """
    return "<i class='fas fa-{name}'></i>".format(name=icon_name)


def lookup(elt, key):
    #if key == 'schedule.day':
        #import pdb
        #pdb.set_trace()
    ret = elt
    for k in key.split('.'):
        try:
            ret = ret[k]
        except:
            return None
    return ret

def extract_group(lst, attr_name, attr_value):
    return [elt for elt in lst if lookup(elt, attr_name) == attr_value]


def compile(src, tgt, ctx):
    tpl = tpl_env.get_template(src)
    open(tgt, 'w').write(tpl.render(ctx))

def mysort(lst, key):
    lst.sort(key=lambda x: x[key])
    return lst

def last_name(data):
    names = data['name'].split(' ')
    last_name = names[-1]
    if len(names) >= 2 and names[-2].lower().strip() in ['de', 'di', 'van', 'von']:
        last_name = names[-2] + ' ' + last_name
    if len(names) >= 3 and names[-3].lower().strip() in ['van', 'von']:
        last_name = names[-3] +' '+names[-2] + ' ' + last_name
    return last_name


MIN_2_PX = 1.8
AM_START = 9*60
PM_START = 14*60+30
def time2px(tm):
    h, m = tm.split(':')
    tm = int(h)*60+int(m)
    if tm >= PM_START:
        return (tm-PM_START)*MIN_2_PX
    else:
        return (tm-AM_START)*MIN_2_PX

def sched2height(start, stop):
    return (time2px(stop)-time2px(start))

def static(path):
    tgt_path = 'static/'+path
    hashsum = hashes.get('build/'+tgt_path, None)
    if hashsum:
        return tgt_path+'?'+hashsum[:20]
    else:
        return tgt_path

# added by Neil

def check_day(talk, day):
    if 'tutorial' in talk:
        for schedule in talk['schedule']:
            if schedule['day'] == day:
                return True
    else:
        return lookup(talk, 'schedule.day') == day

def get_schedule(talk, day):
    if 'tutorial' in talk:
        for schedule in talk['schedule']:
            if schedule['day'] == day:
                return schedule
    else:
        return lookup(talk, 'schedule')

def get_start_number(talk, day):
    tm_string = get_schedule(talk, day)['start']
    h, m = tm_string.split(':')
    return int(h)*60+int(m)

def get_start_number_with_day(talk):
    if talk['title'] == "Ramsification and Semantic Indeterminacy":
        return -1
    if talk['title'] == "Higher cardinal invariants":
        return 1030
    if talk['title'] == "Well-ordering principles in proof theory and reverse mathematics":
        return 21030
    try:
        day = lookup(talk, 'schedule.day')
        tm_string = get_schedule(talk, day)['start']
        h, m = tm_string.split(':')
        d = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'].index(day)
        return d*10000+int(h)*100+int(m)
    except:
        return 100000

def day_sort(lst):
    lst.sort(key = lambda elt: get_start_number_with_day(elt) )
    return lst

def get_short_time_string(talk):
    if talk['title'] == "Ramsification and Semantic Indeterminacy":
        text =  'Sun 18:00'
    elif talk['title'] == "Higher cardinal invariants":
        text =  'Mon-Tue 10:30'
    elif talk['title'] == "Well-ordering principles in proof theory and reverse mathematics":
        text =  'Wed-Thu 10:30'
    else:
        day = lookup(talk, 'schedule.day')
        day_string = day[0:3]
        tm_string = lookup(talk, 'schedule.start')
        text =  day_string+' '+tm_string
        room_number = ['room-0', 'room 1', 'room 2', 'room 3', 'room 4', 'room 5'].index(lookup(talk, 'schedule.room'))
        if room_number != 0:
            text += ', '+['', 'B', 'C', 'D', 'E', 'F'][room_number]
    return '('+text+')'

def get_talks(lst, day, room_number):
    rooms = ['room-0', 'room 1', 'room 2', 'room 3', 'room 4', 'room 5']
    out_lst = []
    for elt in lst:
        if (check_day(elt, day)):
            schedule = get_schedule(elt, day)
            if schedule['room'] == rooms[room_number]:
                out_lst.append(elt)
    out_lst.sort(key = lambda elt: get_start_number(elt, day) )
    return out_lst

def get_session(day, room_number):
    rooms = ['Plenary talks', 'Room B', 'Room C', 'Room D', 'Room E', 'Room F']
    if room_number == 0:
        return rooms[room_number] + ",  " + day + " am"
    else:
        return rooms[room_number] + ",  " + day + " pm"

def get_section(talk, day):
    section = talk['section']
    if 'tutorial' in talk: section = 'Tutorial'
    if section == 'General':
        schedule = get_schedule(talk, day)
        section = schedule['session']
        if section == 'unspecified': section = 'General'
    return section

def get_short_section(talk, day):
    section = get_section(talk, day)
    if section == 'Plenary': section = ''
    elif section == 'Proof Theory and Proof Complexity': section = 'PTPC'
    elif section == 'Reflection Principles and Modal Logic': section = 'RPML'
    elif section == 'Set Theory': section = 'ST'
    elif section == 'Model Theory': section = 'MT'
    elif section == 'Computability': section = 'C'
    elif section == 'Foundations of Geometry': section = 'FG'
    elif section == 'General': section = ''
    if talk['status'] == 'invited' and section != 'Tutorial' and section != '':
        section = '<b>' + section +'*</b>'
    if section != '':
        section = ' ('+section+')'
    return section

def find_metadata(abstracts, surname):
    candidates = []
    surname = surname.lower()
    for abs in abstracts:
        if abs['main_author'].get('lastname', '').lower() == surname:
            return abs
        if surname in abs['main_author'].get('name', '').lower():
            candidates.append(abs)
        elif surname in abs['main_author'].get('fullname', '').lower():
            candidates.append(abs)
    for abs in candidates:
        if surname in abs['main_author'].get('name', '').lower():
            return abs
        if surname in abs['main_author'].get('fullname', '').lower():
            return abs

def get_talk_info(abstracts, surname, key):
    meta = find_metadata(abstracts, surname)
    if not meta:
        return ""
    if key == "title":
        return meta['title']
    if key == "slides":
        return meta.get('slides', [{}])[0].get('path', '')
    if key == "abstract":
        return meta['path']



# end

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compile jinja2 templates.')
    parser.add_argument('src', help='the source template')
    parser.add_argument('out', help='the output filename')
    parser.add_argument('-c', '--context', default=None, help='JSON file with context for the template')
    parser.add_argument('-s', '--hashes', default=None, help='A static file hash list')
    args = parser.parse_args(sys.argv[1:])
    if args.context:
        ctx = json.load(open(args.context, 'r'))
    else:
        ctx = {}
    if args.hashes and Path(args.hashes).exists():
        hashes = {path: hashsum for (hashsum, path) in [ln.strip().split('  ') for ln in open(args.hashes, 'r').readlines()]}
    else:
        hashes = {}

    ctx['extract_group'] = extract_group
    ctx['len'] = len
    ctx['str'] = str
    ctx['int'] = int
    ctx['mysort'] = mysort
    ctx['time2px'] = time2px
    ctx['sched2height'] = sched2height
    ctx['static'] = static
    ctx['last_name'] = last_name
    ctx['json'] = json.dumps

    ctx['get_talks'] = get_talks
    ctx['get_schedule'] = get_schedule
    ctx['get_session'] = get_session
    ctx['get_section'] = get_section
    ctx['get_short_section'] = get_short_section
    ctx['get_short_time_string'] = get_short_time_string
    ctx['get_start_number_with_day'] = get_start_number_with_day
    ctx['day_sort'] = day_sort
    ctx['get_talk_info'] = get_talk_info

    compile(args.src, args.out, ctx)



